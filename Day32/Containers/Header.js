import React, { Component } from "react";
import { TouchableOpacity, Image, StyleSheet, View, Text } from "react-native";
import { Icon, TabBar } from "@ant-design/react-native";
import { withRouter } from "react-router-native";

class Header extends Component {
  goProfile = () => {
    this.props.history.push("/Profile");
  };
  goListProduct = () => {
    this.props.history.push("/Listproduct");
  };

  goAddproduct = () => {
    this.props.history.push("/Addproduct");
  };
  UNSAFE_componentWillMount() {
    console.log(this.props);
  }

  render() {
    return (
      <View style={styles.bar}>
        <View style={styles.Row}>
          <TouchableOpacity>
            <Icon name="bars" size="lg" />
          </TouchableOpacity>
        </View>

        <View style={styles.Row}>
          <TouchableOpacity onPress={this.goListProduct}>
            <Icon name="edit" size="lg" />
            <Text>Product</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.Row}>
          <TouchableOpacity onPress={this.goAddproduct}>
            <Icon name="upload" size="lg" />
            <Text>Add </Text>
            <Text>product</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.Row}>
          <TouchableOpacity onPress={this.goProfile}>
            <Icon name="user" size="lg" />
            <Text>User</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  Row: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 20,
    margin: 10
  },
  bar: {
    flexDirection: "row",
    backgroundColor: "green"
  }
});

export default withRouter(Header);
