import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  Button,
  StyleSheet,
  ImageBackground,
  Image,
  
} from "react-native";
import { InputItem, List } from "@ant-design/react-native";
import backgroundImage from "../Img/bgimg.jpg";
import profileImage from "../Img/pug.jpg";
import {connect} from 'react-redux';
import store from "../Store/store";
const mapStateToProps = state => {
  return {
    Login: state.Login,
    
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onClickLogin: (Username) =>
      dispatch({
        type: 'Login',
        username : Username
      })

  };
}; 
store.subscribe(() => {
  console.log(store.getState());
  
})
class Login extends Component {
  state ={
    users:"",
    password:""


  }
  UNSAFE_componentWillMount(){
    console.log('====================================');
    console.log(this.props.Login);
    console.log('====================================');
  }
 
  goListProduct = () => {
    this.props.onClickLogin(this.state.users)
    this.props.history.push("/Listproduct");
  };
  render() {
   
    return (
      <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
        <View style={styles.Text}>
          <Image source={profileImage} style={styles.profileImage} />
          <Text style={styles.loginText}>Login</Text>
        </View>

        <View>
          <TextInput
            style={styles.input}
            placeholder="Username"
            placeholderTextColor="gray"
            onChangeText={(users) => this.setState({ users })}
          />
        </View>

        <View>
          <TextInput
            style={styles.input}
            placeholder="Password"
            placeholderTextColor="gray"
            secureTextEntry={true} 
            onChangeText={(password) => this.setState({ password })}
          />
        </View>
        <View>
          <Button
            onPress={()=>{this.goListProduct()}}
            title="Login"
            color="#2980B9"
            accessibilityLabel="Learn more about this purple button"
            
          />
        </View>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  backgroundImage: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: null,
    height: null
  },
  profileImage: {
    width: 200,
    height: 200,
    borderRadius: 50,
    justifyContent: "center",
    alignItems: "center"
  },
  input: {
    borderWidth: 1,
    borderRadius: 20,
    height: 40,
    width: 250,
    fontSize: 16,
    paddingLeft: 45,
    margin: 20
    // textAlign:"center"
  },
  loginText: {
    fontSize: 40,
    fontWeight: "500",
    margin: 20
  },
  Text: {
    alignItems: "center"
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);

